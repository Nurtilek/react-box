import React, { Component } from "react";
import "./styles.css";

/*

Каждая кнопка по клику должна окрашить box в свой цвет
Кнопка "Скрыть" по клику должна box'у применить класс box--hide
*/

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "black",
      isHide: false 
    };
  }
  render() {
    return (
      <div className="App">
        <button onClick = {() => {this.setState({color: "red"})}}>Красный</button>
        <button onClick = {() => {this.setState({color: "green"})}}>Зеленый</button>
        <button onClick = {() => {this.setState({color: "blue"})}}>Синий</button>
        <button onClick = {() => {this.setState({isHide: !this.state.isHide})}}>{this.state.isHide ? 'Показать' : 'Скрыть'}</button>

        <div className={this.state.isHide ? 'box--hide' : 'box'} style={{ backgroundColor: this.state.color }} />
      </div>
    );
  }
}

export default App;
